import { AutocompleteInteraction, ChatInputCommandInteraction, Embed, InteractionReplyOptions, SlashCommandBuilder } from "discord.js";
import { AutocompleteCommand, Command } from "../lib/Command";
import { playlists } from "../lib/Playlist";
import { trackFinder } from "../lib/TrackFinder";

export const PlayCommand: AutocompleteCommand = {
    data: new SlashCommandBuilder()
        .setName('play')
        .addStringOption(option => option.setRequired(false).setName("title").setDescription("Search text").setAutocomplete(true))
        .setDescription('Plays a track'),
    autocomplete: async function (interaction: AutocompleteInteraction): Promise<any> {
        const searchString = interaction.options.getString("title");
        console.log(`Searching for ${searchString}`)
        const tracks = await trackFinder.find(searchString, 9);
        const response = tracks
            .map(({ title, url }) => ({
                name: title, value: url
            }));

        await interaction.respond(response)
    },
    execute: async function (interaction: ChatInputCommandInteraction): Promise<any> {
        const searchString = interaction.options.getString("title");
        const playlist = playlists.get(interaction.channel)
        if (!searchString) {
            await playlist.unpause();
            await interaction.reply({ content: `resuming current track`, ephemeral: false } as InteractionReplyOptions);
            return;
        }
        const submitter = await interaction.member.user.username
        const [track] = await trackFinder.find(searchString, 1, submitter);
        await playlist.add(track);
        await interaction.reply({ content: `adding ${track.title} (${track.duration})`, ephemeral: false } as InteractionReplyOptions);
    }
}
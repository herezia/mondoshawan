import { REST, Routes } from "discord.js";
import { CommandsProvider } from "./commands";

const { clientId, guildId, token } = require('./config.json');

const commandsJson = CommandsProvider.getAll().map(it => it.data.toJSON());

// Construct and prepare an instance of the REST module
const rest = new REST({ version: '10' }).setToken(token);

// and deploy your commands!
(async () => {
	try {
		console.log(`Started refreshing ${commandsJson.length} application (/) commands.`);

		// The put method is used to fully refresh all commands in the guild with the current set
		const data = await rest.put(
			Routes.applicationGuildCommands(clientId, guildId),
			{ body: commandsJson },
		);

		console.log(`Successfully reloaded ${(data as Array<unknown>).length} application (/) commands.`);
	} catch (error) {
		// And of course, make sure you catch and log any errors!
		console.error(error);
	}
})();

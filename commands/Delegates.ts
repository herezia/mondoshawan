import { ChatInputCommandInteraction, MessageComponentInteraction, SlashCommandBuilder } from "discord.js";
import { Command } from "../lib/Command";
import { playlists } from "../lib/Playlist";

const definitions = ["skip", "next"].map(it => [it, "Skips to the next track", "next"])
    .concat([
        ["replay", "Replays the current track from the beginning", "replay"],
        ["previous", "Plays the previous track", "previous"],
        ["pause", "Interrupts the current track", "pause"],
        ["unpause", "Interrupts the current track", "unpause"],
    ])
export const DelegatingCommands = definitions
    .map(([name, description, methodName]) => ({
        data: new SlashCommandBuilder()
            .setName(name)
            .setDescription(description),
        execute: async function (interaction: ChatInputCommandInteraction) {
            const playlist = playlists.get(interaction.channel);
            await playlist[methodName]();
            await interaction.reply({ content: `${methodName} done`, ephemeral: false });
        }
    } as Command));
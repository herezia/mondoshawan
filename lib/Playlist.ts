import { Embed, GuildBasedChannel, PartialTextBasedChannelFields, TextChannel } from "discord.js";
import { Player, Track, TrackId } from "./YoutubePlayer";

export class Playlist {
    private player: Player;
    private tracks: Array<Track>;
    private currentTrack?: Track;

    constructor(private channel: GuildBasedChannel & TextChannel) {
        this.tracks = new Array();
        this.player = new Player(channel);
        this.player.onTrackStarted((trackId: TrackId) => {
            const { title, url, thumbnail, duration, submitter: submitterUsername } = this.tracks.find(it => it.id === trackId);
            const { nickname, user } = this.channel.guild.members.cache.find(it => it.user.username === submitterUsername);
            const submitterAvatar = user.displayAvatarURL({ extension: 'jpeg' })
            this.channel.send({
                content: 'now playing',
                embeds: [{
                    title: title,
                    author: {
                        name: nickname, iconURL: submitterAvatar
                    },
                    url,
                    image: {
                        url: thumbnail
                    },
                    fields: [{
                        name: 'Duration', value: `${duration} mn`,
                    }]
                } as Embed]
            });
        });
        this.player.onTrackEnded(() => {
            this.next()
        });
    }

    get currentTrackIndex(): number | undefined {
        const index = this.tracks.indexOf(this.currentTrack);
        return index === -1 ? undefined : index;
    }

    async add(track: Track) {
        this.tracks.push(track);
        if (!this.currentTrack) {
            await this.play(track);
        }
    }

    async next() {
        if (!this.currentTrack)
            return;
        const nextIndex = this.currentTrackIndex + 1;
        if (nextIndex >= this.tracks.length) {
            this.currentTrack = null;
            return;
        }
        await this.playIndex(nextIndex);
    }

    async previous() {
        if (!this.currentTrack)
            return;
        const previousIndex = this.currentTrackIndex - 1;
        if (previousIndex < 0)
            return;
        await this.playIndex(previousIndex);
    }

    async pause() {
        this.player.pause();
    }

    async unpause() {
        this.player.resume();
    }

    async replay() {
        await this.play(this.currentTrack);
    }

    get nextTracks(): Array<Track> {
        return this.tracks.slice(this.currentTrackIndex, this.tracks.length);
    }

    private async playIndex(index: number) {
        await this.play(this.tracks[index]);
    }

    private async play(track: Track) {
        this.currentTrack = track;
        await this.player.play(this.currentTrack);
    }
}

class Playlists {
    private elements: { [name: string]: Playlist };

    constructor() {
        this.elements = {};
    }
    
    get(channel: GuildBasedChannel): Playlist {
        const playlist = this.elements[channel.guildId] || new Playlist(channel as (GuildBasedChannel & TextChannel));
        this.elements[channel.guildId] = playlist;
        return playlist;
    }
}

export const playlists = new Playlists();
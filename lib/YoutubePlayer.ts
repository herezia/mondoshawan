import { AudioPlayer, AudioPlayerError, AudioPlayerPlayingState, AudioPlayerStatus, createAudioPlayer, createAudioResource, getVoiceConnection, joinVoiceChannel } from "@discordjs/voice";
import { GuildBasedChannel } from "discord.js";
import { default as ytdl } from "ytdl-core";

export type TrackId = string;
export interface Track {
    id: TrackId;
    url: string;
    title: string;
    duration: string;
    thumbnail: string;
    submitter: string;
}
export interface YoutubeResourceMetadata {
    id: TrackId;
    title: string;
}

type OnTrackStartedListener = (trackId: TrackId) => void
type OnTrackEndedListener = () => void

export class Player {
    private player: AudioPlayer;
    private trackStartedListeners: Array<OnTrackStartedListener>;
    private trackEndedListeners: Array<OnTrackEndedListener>;

    constructor(private channel: GuildBasedChannel) {
        this.player = createAudioPlayer();
        this.trackStartedListeners = [];
        this.trackEndedListeners = [];
        this.player.on('error', (error: AudioPlayerError) => {
            console.error(`Error: ${error.message} with resource ${JSON.stringify(error.resource.metadata)}`);
        });
        this.player.on(AudioPlayerStatus.Playing, (_, newState: AudioPlayerPlayingState) => {
            this.trackStartedListeners.forEach(fn => fn((newState.resource.metadata as YoutubeResourceMetadata).id));
        });
        this.player.on(AudioPlayerStatus.Idle, () => {
            this.trackEndedListeners.forEach(fn => fn());
        });
    }

    private get voiceConnection() {
        return getVoiceConnection(this.channel.guildId) || joinVoiceChannel({
            channelId: this.channel.id,
            guildId: this.channel.guildId,
            adapterCreator: this.channel.guild.voiceAdapterCreator,
            selfDeaf: false
        });
    }

    async play(track: Track) {
        const stream = await ytdl(track.url, { quality: 'highestaudio' });
        const resource = createAudioResource(stream, { metadata: { title: track.title, id: track.id } as YoutubeResourceMetadata });
        this.player.play(resource);
        this.voiceConnection.subscribe(this.player);
    }

    async pause() {
        this.player.pause(true);
    }

    async resume() {
        this.player.unpause();
    }

    onTrackEnded(fn: OnTrackEndedListener) {
        this.trackEndedListeners.push(fn);
    }

    onTrackStarted(fn: OnTrackStartedListener) {
        this.trackStartedListeners.push(fn);
    }
}

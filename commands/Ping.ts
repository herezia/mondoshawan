import { ChatInputCommandInteraction, InteractionReplyOptions, SlashCommandBuilder } from "discord.js";
import { Command } from "../lib/Command";

export const PingCommand: Command = {
    data: new SlashCommandBuilder()
        .setName('ping')
        .setDescription('Replies with Pong!'),
    execute: async function (interaction: ChatInputCommandInteraction): Promise<any> {
        return interaction.reply({ content: "Pong", ephemeral: false } as InteractionReplyOptions);
    }
}
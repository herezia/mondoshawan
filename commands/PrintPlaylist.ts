import { ChatInputCommandInteraction, InteractionReplyOptions, SlashCommandBuilder } from "discord.js";
import { Command } from "../lib/Command";
import { playlists } from "../lib/Playlist";

export const PrintPlaylist: Command = {
    data: new SlashCommandBuilder()
        .setName('print_playlist')
        .setDescription(`Print tracks that's left to play`),
    execute: async function (interaction: ChatInputCommandInteraction): Promise<any> {
        const playlist = playlists.get(interaction.channel);
        const trackList = playlist.nextTracks.map(it => `  - ${it.title}`).join('\n');
        return interaction.reply({ content: `Playlist content:\n${trackList}`, ephemeral: false } as InteractionReplyOptions);
    }
}
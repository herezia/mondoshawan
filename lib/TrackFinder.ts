import { randomUUID } from "crypto";
import ytsr, { Video } from "ytsr";
import { Track } from "./YoutubePlayer";

class TrackFinder {
    async find(searchString: string, limit = 1, submitter: string = null): Promise<Array<Track>> {
        try {
            const searchResult = await ytsr(searchString, { limit });
            const { items } = searchResult as { items: Video[] };
            
            return items
                .filter(it => it.type === 'video')
                .map(({ url, title, duration, bestThumbnail }) => ({ id: randomUUID(), url, title, duration, thumbnail: bestThumbnail.url, submitter }));
        } catch (e) {
            console.error(e);
        }
    }
}

export const trackFinder = new TrackFinder();
import { AutocompleteInteraction, ChatInputCommandInteraction, Client, CommandInteraction, Events, GatewayIntentBits } from "discord.js";
import { CommandsProvider } from "./commands";
import { token } from "./config.json";

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildVoiceStates] });

client.once(Events.ClientReady, () => {
	console.log('Ready!');
});

client.on(Events.InteractionCreate, async interaction => {
	try {
		if (interaction instanceof ChatInputCommandInteraction) {
			await CommandsProvider.getChatCommand(interaction).execute(interaction);
		} else if (interaction instanceof AutocompleteInteraction) {
			await CommandsProvider.getAutocompleteCommand(interaction).autocomplete(interaction);
		} else {
			return;
		}
	} catch (error) {
		console.error(error);
		if (interaction instanceof CommandInteraction) {
			await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: false });
		}
	}
})

client.login(token);
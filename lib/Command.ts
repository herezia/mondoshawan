import { AutocompleteInteraction, ChatInputCommandInteraction, CommandInteraction, SlashCommandBuilder } from "discord.js";

export interface Command {
    data: SlashCommandBuilder;
    execute: (interaction: ChatInputCommandInteraction) => Promise<any>
}

export interface AutocompleteCommand extends Command {
    autocomplete: (interaction: AutocompleteInteraction) => Promise<any>
}
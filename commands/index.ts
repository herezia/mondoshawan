import { AutocompleteInteraction, ChatInputCommandInteraction } from "discord.js";
import { Command, AutocompleteCommand } from "../lib/Command";
import { DelegatingCommands } from "./Delegates";
import { PingCommand } from "./Ping";
import { PlayCommand } from "./Play";
import { PrintPlaylist } from "./PrintPlaylist";

type Commands = { [name: string]: Command };

const commands = DelegatingCommands.concat(
    [
        PingCommand,
        PlayCommand,
        PrintPlaylist,
    ])
        .reduce((accumulator, value) => {
            accumulator[value.data.name] = value;
            return accumulator;
        }, {} as Commands);

export const CommandsProvider = {
    getAll: () => Object.values(commands),
    getChatCommand: (chatInputCommandInteraction: ChatInputCommandInteraction): Command => {
        return commands[chatInputCommandInteraction.commandName];
    },
    getAutocompleteCommand: (autocompleteInteraction: AutocompleteInteraction): AutocompleteCommand => {
        return commands[autocompleteInteraction.commandName] as AutocompleteCommand;
    }
}